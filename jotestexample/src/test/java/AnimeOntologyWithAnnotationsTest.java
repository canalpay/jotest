/*
 *     Copyright (c) 2017 the original author or authors.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

import org.canalpay.jotest.Assert;
import org.canalpay.jotest.annotations.Match;
import org.canalpay.jotest.annotations.NamespaceBag;
import org.canalpay.jotest.annotations.OntPath;
import org.junit.jupiter.api.Test;

/**
 * This code is part of jotest
 * Created by Can Alpay Çiftci on 14.05.2017.
 */
@OntPath(path = "C:\\OntologyObject", filename = "anime_ontology.owl", source
    = "http://www.canalpay" +
    ".org/hobby/anime/ontology")
@NamespaceBag(mapper = @Match(key = "animeRootUri", value = "http://www" +
    ".canalpay" +
    ".org/hobby/anime/ontology"))
public class AnimeOntologyWithAnnotationsTest {

  @Test
  public void testSuperClassTrue() {
    // Assertions.assertEquals("ee","ee");
    Assert.that("{animeRootUri}#Eser")
        .isSuperClassOf("{animeRootUri}#Anime");
  }

  @Test
  public void testSuperClassFalse() {
    // Assertions.assertEquals("ee","ee");
    Assert.that("http://www.canalpay.org/hobby/anime/ontology#Anime")
        .isSuperClassOf("http://www.canalpay.org/hobby/anime/ontology#Eser");
  }


  @Test
  public void testSuperSuperClassTrue() {
    // Assertions.assertEquals("ee","ee");
    Assert.that("http://www.canalpay.org/hobby/anime/ontology#Eser")
        .isSuperClassOf("http://www.canalpay.org/hobby/anime/ontology#Manga");
  }

  @Test
  public void testSuperSuperClassFalse() {
    // Assertions.assertEquals("ee","ee");
    Assert.thatWithMessage("http://www.canalpay.org/hobby/anime/ontology#Manga",
        "Yanlış olması ilginç biçimde doğru :-P")
        .isSuperClassOf("http://www.canalpay.org/hobby/anime/ontology#Eser");
  }

}
