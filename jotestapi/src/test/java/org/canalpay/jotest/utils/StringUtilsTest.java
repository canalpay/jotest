/*
 *     Copyright (c) 2017 the original author or authors.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package org.canalpay.jotest.utils;

import org.canalpay.jotest.throwable.NoDefinedUriAliasException;
import org.junit.jupiter.api.BeforeAll;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * This code is part of jotest
 * Created by Can Alpay Çiftci on 14.05.2017.
 */
public class StringUtilsTest {

  private static Map<String, String> mapper = new HashMap<>();

  @BeforeAll
  public static void init() {
  }


  @org.junit.jupiter.api.Test
  public void testIsNullOrEmptyWithNullTest() {
    assertTrue(StringUtils.isNullOrEmpty(null));
  }

  @org.junit.jupiter.api.Test
  public void testIsNullOrEmptyWithEmptyTest() {
    assertTrue(StringUtils.isNullOrEmpty(""));
  }

  @org.junit.jupiter.api.Test
  public void testIsNullOrEmptyWithNonNullTest() {
    assertFalse(StringUtils.isNullOrEmpty("returned value shouls be false"));
  }

  @org.junit.jupiter.api.Test
  public void testReplaceKeyToValueTest() {
//    Map<String, String> mapper = new HashMap<>();
    mapper.put("anime", "http://wwww.anime.org");
    mapper.put("manga", "http://wwww.manga.org");

    assertEquals("http://wwww.anime.org#OnePiece", StringUtils
        .replaceKeyToValue("{anime}#OnePiece", mapper));

    assertEquals("http://wwww.manga.org#OnePiece", StringUtils
        .replaceKeyToValue("{manga}#OnePiece", mapper));

    mapper.put("slan", "/sound/language");

    assertEquals("http://wwww.manga.org/sound/language#Turkish", StringUtils
        .replaceKeyToValue("{manga}{slan}#Turkish", mapper));

  }

  @org.junit.jupiter.api.Test
  public void testReplaceKeyToValueShouldThrowException() {

    assertThrows(NoDefinedUriAliasException.class, () -> StringUtils
        .replaceKeyToValue("{mangalar}#OnePiece", mapper));
  }

}
