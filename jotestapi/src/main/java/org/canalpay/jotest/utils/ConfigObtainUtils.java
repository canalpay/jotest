/*
 *     Copyright (c) 2017 the original author or authors.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package org.canalpay.jotest.utils;


import org.canalpay.jotest.annotations.Match;
import org.canalpay.jotest.annotations.NamespaceBag;
import org.canalpay.jotest.annotations.OntPath;
import org.canalpay.jotest.config.JotestConfig;
import org.canalpay.jotest.config.JotestConfigBuilder;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


public class ConfigObtainUtils {
  public static Class<?> getCallerClassName() {
    StackTraceElement[] stElements = Thread.currentThread().getStackTrace();
    String callerClassName = null;
    for (int i = 1; i < stElements.length; i++) {
      StackTraceElement ste = stElements[i];
      if (!ste.getClassName().equals(ConfigObtainUtils.class.getName()) &&
          ste.getClassName().indexOf("java.lang.Thread") != 0) {
        if (callerClassName == null) {
          callerClassName = ste.getClassName();
        } else if (!callerClassName.equals(ste.getClassName())) {
          try {
            Class.forName(ste.getClassName());
            return Class.forName(ste.getClassName());
          } catch (ClassNotFoundException e) {
            e.printStackTrace();
          }
        }
      }
    }
    return null;
  }

  public static Class<?> getCallerClassNameForAnnotation() {
    StackTraceElement[] stElements = Thread.currentThread().getStackTrace();
    try {
      return Class.forName(stElements[6].getClassName());
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static Optional<JotestConfig> getMethotedConfigAsJotestConfig(final
                                                                       Class<?> clazz) {
    final Object newInstance;

    try {
      newInstance = clazz.newInstance();
    } catch (IllegalAccessException | InstantiationException e) {
      e.printStackTrace();
      return Optional.empty();
    }
    final JotestConfig retVal;

    try {
      retVal = (JotestConfig) clazz.getDeclaredMethod("getConfig", null)
          .invoke(newInstance, null);
    } catch (IllegalAccessException | NoSuchMethodException |
        InvocationTargetException e) {
//      e.printStackTrace();
      return Optional.empty();
    }

    return Optional.ofNullable(retVal);
  }

  public static String getMethodName() {
    return Thread.currentThread().getStackTrace()[2].getMethodName();
  }

  public static Optional<JotestConfig> getAnnotatedConfigAsJotestConfig(final
                                                                        Class<?> clazz) {

    Annotation annotationOnPath = clazz.getAnnotation(OntPath.class);
    OntPath ontPath = (OntPath) annotationOnPath;

    if (ontPath == null) {
      return Optional.empty();
    }

    JotestConfigBuilder jotestConfigBuilder = new JotestConfigBuilder();

    if (!StringUtils.isNullOrEmpty(ontPath.filename())) {
      jotestConfigBuilder.setFilename(ontPath.filename());
    }
    if (!StringUtils.isNullOrEmpty(ontPath.filetype())) {
      jotestConfigBuilder.setFiletype(ontPath.filetype());
    }
    //Todo ns öğesine bak.  Muhtemelen Source ile aynı
    //    if (!StringUtils.isNullOrEmpty(ontPath.ns())) {
//      jotestConfigBuilder = jotestConfigBuilder.set(ontPath.ns());
//    }
    if (!StringUtils.isNullOrEmpty(ontPath.onttype())) {
      jotestConfigBuilder.setOnttype(ontPath.onttype());
    }
    if (!StringUtils.isNullOrEmpty(ontPath.path())) {
      jotestConfigBuilder.setPath(ontPath.path());
    }
    if (!StringUtils.isNullOrEmpty(ontPath.source())) {
      jotestConfigBuilder.setSource(ontPath.source());
    }

    Annotation annotationNamespaceBag = clazz.getAnnotation(NamespaceBag.class);

    NamespaceBag namespaceBag = (NamespaceBag) annotationNamespaceBag;

    Map<String, String> namespaceBagMap = new HashMap<>();

    if (namespaceBag.mapper() != null) {
      for (Match match : namespaceBag.mapper()) {
        namespaceBagMap.put(match.key(), match.value());
      }
    }

    jotestConfigBuilder.setNamespaceBag(namespaceBagMap);


    return Optional.of(jotestConfigBuilder.createConfig());
  }
}
