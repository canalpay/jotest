/*
 *     Copyright (c) 2017 the original author or authors.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package org.canalpay.jotest.utils;

import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.ontology.DatatypeProperty;
import org.apache.jena.ontology.InverseFunctionalProperty;
import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.ontology.SymmetricProperty;
import org.apache.jena.ontology.TransitiveProperty;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.canalpay.jotest.config.JotestConfig;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class OntologyCreator {
  //OntModel ontModel;

  private static Logger logger = LogManager.getLogger(OntologyCreator.class);
  private static JotestConfig jotestConfig;
  private OntModel ontModel;

  public OntologyCreator(final JotestConfig jotestConfig) {
    OntologyCreator.jotestConfig = jotestConfig;
    this.ontModel = jotestConfig.getOntModel();
  }

  public static void saveToFile(final OntModel ontModel) {
    FileOutputStream fileOutputStream;
    try {
      fileOutputStream = new FileOutputStream(jotestConfig.getFilename());
    } catch (FileNotFoundException e) {
      e.printStackTrace();
      return;
    }
    ontModel.write(fileOutputStream, jotestConfig.getFiletype());
    logger.info("OWL File is written :" + jotestConfig.getFilename());
  }

  public static void printToConsole(OntModel ontModel) {
    ontModel.write(System.out, jotestConfig.getFiletype());
  }

  public OntologyCreator addClass(final String name) {
    ontModel.createClass(name);
    return this;
  }

  public OntologyCreator addDataProperty(final String name, final
  List<String> domainClasses, final Resource range,
                                         final boolean isFunctional) {
    DatatypeProperty datatypeProperty =
        ontModel.createDatatypeProperty(name, isFunctional);

    domainClasses.forEach(dC -> datatypeProperty.addDomain(ontModel
        .getOntClass(dC)));
    datatypeProperty.addRange(range);

    return this;
  }

  public OntologyCreator addObjectPropertyViaString(final String name,
                                                    final PropertyTypes
                                                        .ObjectTypes
                                                        objectTypes,
                                                    final List<String>
                                                        domainClasses,
                                                    final List<String>
                                                        rangeClasses, final
                                                    boolean isFunctional) {
    List<OntClass> ontDomainClasses = new ArrayList<>();
    List<OntClass> ontRangeClasses = new ArrayList<>();

    domainClasses.forEach(dC -> ontDomainClasses.add(ontModel.getOntClass(dC)));
    rangeClasses.forEach(rC -> ontRangeClasses.add(ontModel.getOntClass(rC)));

    addObjectProperty(name, objectTypes, ontDomainClasses, ontRangeClasses,
        isFunctional);

    return this;
  }

  public OntologyCreator addObjectPropertyViaString(final String name,
                                                    final PropertyTypes
                                                        .ObjectTypes
                                                        objectTypes,
                                                    final String domainClass,
                                                    final String rangeClass,
                                                    final boolean
                                                        isFunctional) {

    addObjectPropertyViaString(name, objectTypes, Arrays.asList(domainClass),
        Arrays.asList(rangeClass), isFunctional);

    return this;
  }

  public OntologyCreator addToModelIndividualType(final String subjectName,
                                                  final String typeClass) {

    ontModel.add(ontModel.getResource(individualNameGenerator(subjectName)),
        RDF.type,
        ontModel.getOntClass(typeClass));

    return this;
  }

  public OntologyCreator addToModelAsObjectPropertyTriple(final String
                                                              subjectName,
                                                          final String
                                                              predictName,
                                                          final String
                                                              objectName
  ) {
    // ClassNameOfObject yalnızca isim üretim içindir

    ontModel.add(ontModel.getResource(individualNameGenerator(subjectName)),
        ontModel.getProperty(predictName),
        ontModel.getResource(individualNameGenerator(objectName)));

    return this;
  }

  public OntologyCreator addToModelAsDataPropertyTriple(final String
                                                            subjectName,
                                                        final String
                                                            predictName,
                                                        final Object value,
                                                        final XSDDatatype type
  ) {
    // ClassNameOfObject yalnızca isim üretim içindir

    ontModel.addLiteral(ontModel.getResource(individualNameGenerator
            (subjectName)),
        ontModel.getProperty(predictName),
        ontModel.createTypedLiteral(value, type));

    return this;
  }

  public OntologyCreator addIndividual(final String name, final String
      className) {
    ontModel.createIndividual(individualNameGenerator(name),
        ontModel.getResource(className));

    return this;
  }

  /// TODO implement this function
  private String individualNameGenerator(final String name) {
    String newName =
        name.replace(" ", "_").replace(";", "_0_").replace(",", "_1_")
            .replace(".", "_2_");
    return "IND_" /* + className.substring(3) // Maybe Later */ + "_" + newName;
  }

  public OntologyCreator addSubObjectProperty(final String
                                                  superObjectPropertyName,
                                              final List<String> names
  ) {
    OntProperty objectProperty = ontModel.getOntProperty
        (superObjectPropertyName);
    names.forEach(name -> objectProperty.addSubProperty(ontModel
        .getOntProperty(name)));
    return this;
  }

  public OntologyCreator addObjectProperty(final String name, final
  PropertyTypes.ObjectTypes objectTypes,
                                           final List<OntClass> domainClass,
                                           final List<OntClass> rangeClass,
                                           final boolean isFunctional) {
    if (objectTypes.equals(PropertyTypes.ObjectTypes
        .createInverseFunctionalProperty)) {
      InverseFunctionalProperty inverseFunctionalProperty =
          ontModel.createInverseFunctionalProperty(name, isFunctional);
      domainClass.forEach(inverseFunctionalProperty::addDomain);
      rangeClass.forEach(inverseFunctionalProperty::addRange);
    } else if (objectTypes.equals(PropertyTypes.ObjectTypes
        .createObjectProperty)) {
      ObjectProperty objectProperty = ontModel.createObjectProperty(name,
          isFunctional);
      domainClass.forEach(objectProperty::addDomain);
      rangeClass.forEach(objectProperty::addRange);
    } else if (objectTypes.equals(PropertyTypes.ObjectTypes
        .createSymmetricProperty)) {
      SymmetricProperty symmetricProperty =
          ontModel.createSymmetricProperty(name, isFunctional);
      domainClass.forEach(symmetricProperty::addDomain);
      rangeClass.forEach(symmetricProperty::addRange);
    } else if (objectTypes.equals(PropertyTypes.ObjectTypes
        .TransitiveProperty)) {
      TransitiveProperty transitiveProperty =
          ontModel.createTransitiveProperty(name, isFunctional);
      domainClass.forEach(transitiveProperty::addDomain);
      rangeClass.forEach(transitiveProperty::addRange);
    }
    return this;
  }

  public OntologyCreator addAllClasses(final List<String> names) {
    names.forEach(name -> ontModel.createClass(name));
    return this;
  }

  public OntologyCreator addSubClass(final String superClassName, final
  String name) {
    ontModel.getOntClass(superClassName)
        .addSubClass(ontModel.createClass(name));
    return this;
  }

  public OntologyCreator addAllSubClasses(final String superClassName, final
  List<String> names) {
    names.forEach(name -> addSubClass(superClassName, name));
    return this;
  }
}
