/*
 *     Copyright (c) 2017 the original author or authors.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package org.canalpay.jotest.utils;

import org.canalpay.jotest.throwable.NoDefinedUriAliasException;

import java.util.Map;

/**
 * This code is part of jotest
 * Created by Can Alpay Çiftci on 14.05.2017.
 */
public class StringUtils {

  public static boolean isNullOrEmpty(final String string) {
    return string == null || string.isEmpty();
  }

  public static String replaceKeyToValue(String string, Map<String, String>
      mapper) {

    StringBuilder stringBuilder = new StringBuilder();

    int counter = 0;

    int startingIndices = 0;

    int endingIndices = 0;

    for (char character : string.toCharArray()) {
      if (character == '{') {
        startingIndices = counter;
      }

      if (character == '}') {
        stringBuilder.append(string.substring(endingIndices, startingIndices));

        String key = string.substring(startingIndices + 1, counter);
        String value = mapper.get(key);

        if (value == null) {
          throw new NoDefinedUriAliasException("The uri of " + key + " is not" +
              " " +
              "found");
        }

        stringBuilder.append(value);

        endingIndices = counter + 1;
      }

      counter++;
    }

    stringBuilder.append(string.substring(endingIndices, counter));

    return stringBuilder.toString();

  }
}
