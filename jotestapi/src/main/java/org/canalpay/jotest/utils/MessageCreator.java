/*
 *     Copyright (c) 2017 the original author or authors.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package org.canalpay.jotest.utils;

import org.canalpay.jotest.core.MessageContainer;

public class MessageCreator {
  private MessageContainer messageContainer;

  private MessageCreator(final MessageContainer messageContainer) {
    this.messageContainer = messageContainer;
  }

  public static MessageCreator create(final MessageContainer messageContainer) {
    return new MessageCreator(messageContainer);
  }

  private StringBuilder appendIfStringExist(final StringBuilder
                                                stringBuilder, final String
                                                description,
                                            final String operation) {
    if (StringUtils.isNullOrEmpty(description) || StringUtils.isNullOrEmpty
        (operation)) {
      return stringBuilder;
    }

    stringBuilder.append(description);
    stringBuilder.append(" ");
    stringBuilder.append(operation);
    stringBuilder.append("\n");


    return stringBuilder;
  }

  public String getMessage() {
    StringBuilder stringBuilder = new StringBuilder();

    stringBuilder =
        appendIfStringExist(stringBuilder, "Assertion Name :",
            messageContainer.assertionName);

    stringBuilder =
        appendIfStringExist(stringBuilder, "User Message :", messageContainer
            .userMessage);

    stringBuilder =
        appendIfStringExist(stringBuilder, "First uri :", messageContainer.uri);

    stringBuilder =
        appendIfStringExist(stringBuilder, "Second uri :", messageContainer
            .nextUri);

    stringBuilder =
        appendIfStringExist(stringBuilder, "Expected :", messageContainer
            .expected);

    stringBuilder =
        appendIfStringExist(stringBuilder, "Actual uri :", messageContainer
            .actual);

    return stringBuilder.toString();
  }

}
