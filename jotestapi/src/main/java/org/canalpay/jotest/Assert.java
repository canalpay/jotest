/*
 *     Copyright (c) 2017 the original author or authors.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package org.canalpay.jotest;

import org.canalpay.jotest.config.JotestConfig;
import org.canalpay.jotest.core.MessageContainer;
import org.canalpay.jotest.core.OntologyTester;
import org.canalpay.jotest.throwable.NoConfigFoundException;
import org.canalpay.jotest.throwable.OntAssertionError;
import org.canalpay.jotest.utils.ConfigObtainUtils;
import org.canalpay.jotest.utils.MessageCreator;
import org.canalpay.jotest.utils.StringUtils;

public class Assert {
  private String uri;
  private String userMessage;
  private JotestConfig jotestConfig;

  private Assert(final String uri, final String userMessage) {
    jotestConfig = ConfigObtainUtils.getMethotedConfigAsJotestConfig
        (ConfigObtainUtils.getCallerClassName()).orElseGet(()
        -> ConfigObtainUtils.getAnnotatedConfigAsJotestConfig
        (ConfigObtainUtils.getCallerClassNameForAnnotation()).orElseThrow(
        () -> new NoConfigFoundException("Jotest configurations is noot " +
            "fcound" +
            ".")));
    this.uri = StringUtils.replaceKeyToValue(uri, jotestConfig
        .getNamespaceBag());
    this.userMessage = userMessage;

  }

  public static Assert that(final String uri) {
    return new Assert(uri, null);
  }

  public static Assert thatWithMessage(final String uri, final String
      userMessage) {
    return new Assert(uri, userMessage);
  }

  @Deprecated
  public Assert configure(JotestConfig jotestConfig) {

    this.jotestConfig = jotestConfig;
    return this;
  }

  public void isSuperClassOf(final String subClassUri) {
    String handledSubClassUri = StringUtils.replaceKeyToValue(subClassUri,
        jotestConfig.getNamespaceBag());
    if (new OntologyTester(jotestConfig).isSuperClassOf(uri,
        handledSubClassUri)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            handledSubClassUri, "is not superclass",
            "must be super class", userMessage)).getMessage());
  }

  public void isSubClassOf(final String nextUri) {
    String handledNextUri = StringUtils.replaceKeyToValue(nextUri,
        jotestConfig.getNamespaceBag());
    if (new OntologyTester(jotestConfig).isSubClassOf(uri, handledNextUri)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            handledNextUri, null,
            null, userMessage)).getMessage());
  }

  public void isEquivalentClassOf(final String nextUri) {
    String handledNextUri = StringUtils.replaceKeyToValue(nextUri,
        jotestConfig.getNamespaceBag());
    if (new OntologyTester(jotestConfig).isEquivalentClassOf(uri, handledNextUri)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            handledNextUri, null,
            null, userMessage)).getMessage());
  }

  public void isPropertyOf(final String nextUri) {
    String handledNextUri = StringUtils.replaceKeyToValue(nextUri,
        jotestConfig.getNamespaceBag());
    if (new OntologyTester(jotestConfig).isPropertyOf(uri, handledNextUri)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            handledNextUri, null,
            null, userMessage)).getMessage());
  }

  public void isUriOf(final String nextUri) {
    String handledNextUri = StringUtils.replaceKeyToValue(nextUri,
        jotestConfig.getNamespaceBag());
    if (new OntologyTester(jotestConfig).isUriOf(uri, handledNextUri)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            handledNextUri, null,
            null, userMessage)).getMessage());
  }

  public void hasLabel(final String nextUri, final Object literal) {
    String handledNextUri = StringUtils.replaceKeyToValue(nextUri,
        jotestConfig.getNamespaceBag());
    if (new OntologyTester(jotestConfig).hasLabel(handledNextUri, nextUri, literal)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            handledNextUri, null,
            null, userMessage)).getMessage());
  }

  public void hasLabel(final String nextUri, final int literal) {
    String handledNextUri = StringUtils.replaceKeyToValue(nextUri,
        jotestConfig.getNamespaceBag());
    if (new OntologyTester(jotestConfig).hasLabel(uri, handledNextUri, literal)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            handledNextUri, null,
            null, userMessage)).getMessage());
  }

  public void hasLabel(final String nextUri, final float literal) {
    String handledNextUri = StringUtils.replaceKeyToValue(nextUri,
        jotestConfig.getNamespaceBag());
    if (new OntologyTester(jotestConfig).hasLabel(uri, handledNextUri,
        literal)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            handledNextUri, null,
            null, userMessage)).getMessage());
  }

  public void hasLabel(final String nextUri, final char literal) {
    String handledNextUri = StringUtils.replaceKeyToValue(nextUri,
        jotestConfig.getNamespaceBag());
    if (new OntologyTester(jotestConfig).hasLabel(uri, handledNextUri, literal)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            handledNextUri, null,
            null, userMessage)).getMessage());
  }

  public void hasLabel(final String nextUri, final double literal) {
    String handledNextUri = StringUtils.replaceKeyToValue(nextUri,
        jotestConfig.getNamespaceBag());
    if (new OntologyTester(jotestConfig).hasLabel(uri, handledNextUri,
        literal)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            handledNextUri, null,
            null, userMessage)).getMessage());
  }

  public void hasLabel(final String nextUri, final long literal) {
    String handledNextUri = StringUtils.replaceKeyToValue(nextUri,
        jotestConfig.getNamespaceBag());
    if (new OntologyTester(jotestConfig).hasLabel(uri, handledNextUri, literal)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            handledNextUri, null,
            null, userMessage)).getMessage());
  }

  public void isVersionInfoOf(final String info) {
    if (new OntologyTester(jotestConfig).hasVersionInfo(uri, info)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            null, null,
            null, userMessage)).getMessage());
  }

  public void hasLabel(final String label, final String language) {
    if (new OntologyTester(jotestConfig).hasLabel(uri, label,
        language)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            null, null,
            null, userMessage)).getMessage());
  }

  public void isSeeAlsoOf(final String nextUri) {
    String handledNextUri = StringUtils.replaceKeyToValue(nextUri,
        jotestConfig.getNamespaceBag());
    if (new OntologyTester(jotestConfig).isSeeAlsoOf(uri, handledNextUri)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            handledNextUri, null,
            null, userMessage)).getMessage());
  }

  public void isComplementClass() {
    if (new OntologyTester(jotestConfig).isComplementClass(uri)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            null, null,
            null, userMessage)).getMessage());
  }

  public void isDisjointWith(final String nextUri) {
    String handledNextUri = StringUtils.replaceKeyToValue(nextUri,
        jotestConfig.getNamespaceBag());
    if (new OntologyTester(jotestConfig).isDisjointWith(uri, handledNextUri)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            handledNextUri, null,
            null, userMessage)).getMessage());
  }

  public void isEnumeratedClass() {
    if (new OntologyTester(jotestConfig).isEnumeratedClass(uri)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            null, null,
            null, userMessage)).getMessage());
  }

  public void isIntersectionClass() {
    if (new OntologyTester(jotestConfig).isIntersectionClass(uri)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            null, null,
            null, userMessage)).getMessage());
  }

  public void isRestriction() {
    if (new OntologyTester(jotestConfig).isRestriction(uri)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            null, null,
            null, userMessage)).getMessage());
  }

  public void isUnionClass() {
    if (new OntologyTester(jotestConfig).isUnionClass(uri)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            null, null,
            null, userMessage)).getMessage());
  }

  public void isDataRange() {
    if (new OntologyTester(jotestConfig).isDataRange(uri)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            null, null,
            null, userMessage)).getMessage());
  }

  public void isDatatypeProperty() {
    if (new OntologyTester(jotestConfig).isDatatypeProperty(uri)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            null, null,
            null, userMessage)).getMessage());
  }

  public void isAnnotationProperty() {
    if (new OntologyTester(jotestConfig).isAnnotationProperty(uri)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            null, null,
            null, userMessage)).getMessage());
  }

  public void isObjectProperty() {
    if (new OntologyTester(jotestConfig).isObjectProperty(uri)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            null, null,
            null, userMessage)).getMessage());
  }

  public void isAnon() {
    if (new OntologyTester(jotestConfig).isAnon(uri)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            null, null,
            null, userMessage)).getMessage());
  }

  public void isClass() {
    if (new OntologyTester(jotestConfig).isClass(uri)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            null, null,
            null, userMessage)).getMessage());
  }

  public void isIndividual() {
    if (new OntologyTester(jotestConfig).isIndividual(uri)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            null, null,
            null, userMessage)).getMessage());
  }

  public void isDifferentFrom(final String nextUri) {
    if (new OntologyTester(jotestConfig).isDifferentFrom(uri, nextUri)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            nextUri, null,
            null, userMessage)).getMessage());
  }

  public void isSameAs(final String nextUri) {
    if (new OntologyTester(jotestConfig).isSameAs(uri, nextUri)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            nextUri, null,
            null, userMessage)).getMessage());
  }

  public void isResource() {
    if (new OntologyTester(jotestConfig).isResource(uri)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            null, null,
            null, userMessage)).getMessage());
  }

  public void isLiteral() {
    if (new OntologyTester(jotestConfig).isLiteral(uri)) {
      return;
    }
    throw new OntAssertionError(MessageCreator.create(
        new MessageContainer(ConfigObtainUtils.getMethodName(), uri,
            null, null,
            null, userMessage)).getMessage());
  }

}
