/*
 *     Copyright (c) 2017 the original author or authors.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package org.canalpay.jotest.core;

import java.io.Serializable;

public class MessageContainer implements Serializable {
  public String assertionName;

  public String uri;
  public String nextUri;

  public String actual;
  public String expected;

  public String userMessage;

  public MessageContainer(final String assertionName, final String uri, final
  String nextUri, final String actual,
                          final String expected, final String userMessage) {
    this.assertionName = assertionName;
    this.uri = uri;
    this.nextUri = nextUri;
    this.actual = actual;
    this.expected = expected;
    this.userMessage = userMessage;
  }
}
