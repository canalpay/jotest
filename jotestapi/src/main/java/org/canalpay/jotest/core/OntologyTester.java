/*
 *     Copyright (c) 2017 the original author or authors.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package org.canalpay.jotest.core;

import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.FileManager;
import org.canalpay.jotest.config.JotestConfig;
import org.canalpay.jotest.throwable.NoUriFoundException;

import java.io.InputStream;

public class OntologyTester {

  private OntModel ontModel;

  public OntologyTester(final JotestConfig jotestConfig) {
    if (jotestConfig.getOntModel() != null) {
      ontModel = jotestConfig.getOntModel();
    } else {
      ontModel = ModelFactory.createOntologyModel();
      InputStream fileInputStream =
          FileManager.get().open(jotestConfig.getPath() + "\\" + jotestConfig
              .getFilename());

      ontModel.read(fileInputStream, null);
    }

  }

  private OntClass getOntClassOrThrow(final String uri) {
    OntClass ontClass = ontModel.getOntClass(uri);
    if (ontClass == null) {
      throw new NoUriFoundException(uri + " is not found!");
    }
    return ontClass;
  }

  public boolean isSuperClassOf(final String uri, final String nextUri) {
    return getOntClassOrThrow(uri).hasSubClass(getOntClassOrThrow(nextUri));
  }

  public boolean isSubClassOf(final String uri, final String nextUri) {
    return getOntClassOrThrow(uri).hasSuperClass(ontModel.getOntClass
        (nextUri));
  }


  public boolean isEquivalentClassOf(final String uri, final String nextUri) {
    return getOntClassOrThrow(uri).hasEquivalentClass(ontModel.getOntClass
        (nextUri));
  }

  public boolean isPropertyOf(final String uri, final String nextUri) {
    return getOntClassOrThrow(nextUri).hasProperty(ontModel.getProperty(uri));
  }

  public boolean isUriOf(final String uri, final String nextUri) {
    return getOntClassOrThrow(nextUri).hasURI(uri);
  }

  public boolean hasLabel(final String uri, final String nextUri, final
  Object literal) {
    return getOntClassOrThrow(nextUri).hasLiteral(ontModel.getProperty(uri)
        , literal);
  }

  public boolean hasLabel(final String uri, final String nextUri, final int
      literal) {
    return getOntClassOrThrow(nextUri).hasLiteral(ontModel.getProperty(uri)
        , literal);
  }

  public boolean hasLabel(final String uri, final String nextUri, final float
      literal) {
    return getOntClassOrThrow(nextUri).hasLiteral(ontModel.getProperty(uri)
        , literal);
  }

  public boolean hasLabel(final String uri, final String nextUri, final char
      literal) {
    return getOntClassOrThrow(nextUri).hasLiteral(ontModel.getProperty(uri)
        , literal);
  }

  public boolean hasLabel(final String uri, final String nextUri, final
  double literal) {
    return getOntClassOrThrow(nextUri).hasLiteral(ontModel.getProperty(uri)
        , literal);
  }

  public boolean hasLabel(final String uri, final String nextUri, final long
      literal) {
    return getOntClassOrThrow(nextUri).hasLiteral(ontModel.getProperty(uri)
        , literal);
  }

  public boolean hasLabel(final String nextUri, final String label, final
  String
      language) {
    return getOntClassOrThrow(nextUri).hasLabel(label, language);
  }

  public boolean hasVersionInfo(final String nextUri, final String info) {
    return getOntClassOrThrow(nextUri).hasVersionInfo(info);
  }

  public boolean isSeeAlsoOf(final String uri, final String nextUri) {
    return getOntClassOrThrow(nextUri).hasSeeAlso(ontModel.getResource(uri));
  }

  public boolean isComplementClass(final String uri) {
    return getOntClassOrThrow(uri).isComplementClass();
  }

  public boolean isDisjointWith(final String uri, final String nextUri) {
    return getOntClassOrThrow(uri).isDisjointWith(ontModel.getResource
        (nextUri));
  }

  public boolean isEnumeratedClass(final String uri) {
    return getOntClassOrThrow(uri).isEnumeratedClass();
  }

  public boolean isIntersectionClass(final String uri) {
    return getOntClassOrThrow(uri).isIntersectionClass();
  }

  public boolean isRestriction(final String uri) {
    return getOntClassOrThrow(uri).isRestriction();
  }

  public boolean isUnionClass(final String uri) {
    return getOntClassOrThrow(uri).isUnionClass();
  }

  public boolean isDataRange(final String uri) {
    return getOntClassOrThrow(uri).isDataRange();
  }

  public boolean isDatatypeProperty(final String uri) {
    return getOntClassOrThrow(uri).isDatatypeProperty();
  }

  public boolean isAnnotationProperty(final String uri) {
    return getOntClassOrThrow(uri).isAnnotationProperty();
  }

  public boolean isObjectProperty(final String uri) {
    return getOntClassOrThrow(uri).isAnnotationProperty();
  }

  public boolean isAnon(final String uri) {
    return getOntClassOrThrow(uri).isAnon();
  }

  public boolean isClass(final String uri) {
    return getOntClassOrThrow(uri).isClass();
  }

  public boolean isIndividual(final String uri) {
    return getOntClassOrThrow(uri).isIndividual();
  }

  public boolean isDifferentFrom(final String uri, final String nextUri) {
    return getOntClassOrThrow(uri).isDifferentFrom(ontModel.getResource
        (nextUri));
  }


  public boolean isSameAs(final String uri, final String nextUri) {
    return getOntClassOrThrow(uri).isSameAs(ontModel.getResource(nextUri));
  }


  public boolean isResource(final String uri) {
    return getOntClassOrThrow(uri).isResource();
  }

  public boolean isLiteral(final String uri) {
    return getOntClassOrThrow(uri).isLiteral();
  }


  //    ontModel.getOntClass("www").isComplementClass()
  //    ontModel.getOntClass("www").isDisjointWith()
  //    ontModel.getOntClass("www").isEnumeratedClass()
  //    ontModel.getOntClass("www").isIntersectionClass()
  //    ontModel.getOntClass("www").isRestriction()
  //    ontModel.getOntClass("www").isUnionClass()
  //    ontModel.getOntClass("www").isDataRange()
  //    ontModel.getOntClass("www").isDatatypeProperty()
  //    ontModel.getOntClass("www").isAnnotationProperty()
  //    ontModel.getOntClass("www").isObjectProperty()
  //    ontModel.getOntClass("www").isAnon()
  //    ontModel.getOntClass("www").isClass(),
  //    ontModel.getOntClass("www").isIndividual()
  //    ontModel.getOntClass("www").isDifferentFrom()
  //    ontModel.getOntClass("www").isSameAs()
  //    ontModel.getOntClass("www").isResource()
  //    ontModel.getOntClass("www").isLiteral()


  //  public boolean isCommentOf(String uri, String nextUri) {
  //    return getOntClassOrThrow(nextUri).hasComment(ontModel.getRDFNode
  // (uri));
  //  }


  //  getOntClassOrThrow(uri).hasEquivalentClass();
  // Todo    getOntClassOrThrow(uri).hasComment(); çalışmıyor. literal istiyor
  // getOntClassOrThrow(uri).hasProperty();
  //  getOntClassOrThrow(uri).hasURI();
  //  getOntClassOrThrow(uri).hasLiteral();
  //  getOntClassOrThrow(uri).hasLabel();
  // getOntClassOrThrow(uri).hasVersionInfo();
  // getOntClassOrThrow(uri).hasSeeAlso();


}
