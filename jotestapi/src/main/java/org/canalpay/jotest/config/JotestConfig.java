/*
 *     Copyright (c) 2017 the original author or authors.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package org.canalpay.jotest.config;

import org.apache.jena.ontology.OntModel;
import org.apache.logging.log4j.Level;

import java.util.Collections;
import java.util.Map;

public class JotestConfig {


  private final String source;
  private final OntModel ontModel;
  private final String ns;

  private final String filename;
  private final String path;
  private final String filetype;
  private final String onttype;
  private final Level level;

  private final Map<String, String> namespaceBag;

  JotestConfig(final String source, final OntModel ontModel, final String
      filename, final String path, final String filetype,
               final String onttype, final Level level, final Map<String,
      String>
                   namespaceBag) {
    this.source = source;
    this.ontModel = ontModel;
    this.ns = source + "#";
    this.filename = filename;
    this.path = path;
    this.filetype = filetype;
    this.onttype = onttype;
    this.level = level;
    if (namespaceBag == null) {
      this.namespaceBag = Collections.emptyMap();
    } else {
      this.namespaceBag = namespaceBag;
    }

  }

  public String getSource() {
    return source;
  }

  public OntModel getOntModel() {
    return ontModel;
  }

  public String getNs() {
    return ns;
  }

  public String getFilename() {
    return filename;
  }

  public String getPath() {
    return path;
  }

  public String getFiletype() {
    return filetype;
  }

  public String getOnttype() {
    return onttype;
  }

  public Level getLevel() {
    return level;
  }

  public Map<String, String> getNamespaceBag() {
    return namespaceBag;
  }

  //  public static final String SOURCE = "http://www.canalpay
  // .org/hobby/anime/ontology";
  //  public static final String NS = SOURCE + "#";
  //
  //  public static final String FILENAME = "anime_ontology";
  //  public static final String FILENAME_WITH_EXTENSION = FILENAME + ".owl";
  //
  //  public static final String PATH = "C:\\OntologyObject\\src\\main\\java";
  //  public static final String FILETYPE = "RDF/XML-ABBREV";
}
