/*
 *     Copyright (c) 2017 the original author or authors.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package org.canalpay.jotest.config;

import org.apache.jena.ontology.OntModel;
import org.apache.logging.log4j.Level;

import java.util.Map;

public class JotestConfigBuilder {
  private String source;
  private OntModel ontModel;
  private String filename;
  private String path;
  private String filetype;
  private String onttype;
  private Level level;
  private Map<String, String> namespaceBag;


  public JotestConfigBuilder setSource(final String source) {
    this.source = source;
    return this;
  }

  public JotestConfigBuilder setOntModel(final OntModel ontModel) {
    this.ontModel = ontModel;
    return this;
  }

  public JotestConfigBuilder setFilename(final String filename) {
    this.filename = filename;
    return this;
  }

  public JotestConfigBuilder setPath(final String path) {
    this.path = path;
    return this;
  }

  public JotestConfigBuilder setFiletype(final String filetype) {
    this.filetype = filetype;
    return this;
  }

  public JotestConfigBuilder setOnttype(final String onttype) {
    this.onttype = onttype;
    return this;
  }

  public JotestConfigBuilder setLevel(final Level level) {
    this.level = level;
    return this;
  }

  public JotestConfigBuilder setNamespaceBag(final Map<String, String>
                                                 namespaceBag) {
    this.namespaceBag = namespaceBag;
    return this;
  }

  public JotestConfig createConfig() {
    return new JotestConfig(source, ontModel, filename, path, filetype,
        onttype, level, namespaceBag);
  }
}
